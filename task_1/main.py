import sys
import json

def merge_arrays(arr1, arr2):
    result = [x for x in arr1 if x in arr2]
    return result

if __name__ == "__main__":
    array1_str = sys.argv[1]
    array2_str = sys.argv[2]
    
    array1 = json.loads(array1_str)
    array2 = json.loads(array2_str)
    
    result = merge_arrays(array1, array2)
    
    print("Исходный массив 1:", array1)
    print("Исходный массив 2:", array2)
    print("Вывод:", result)
